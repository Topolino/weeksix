# Name: Joey Carberry
# Date: October 6, 2016
# Project: Project6 using car.py
import car

year = input('What is the year of your car? ')
model = input('What is the model of your car? ')
velocity = input('What is the starting speed of your car? ')
userCar = car.Car(year, model, velocity)

print (userCar.get_velocity(), 'km/h')

def accelerateCar():
    for x in range(0,5):
        userCar.accelerate()
        print (userCar.get_velocity(), 'km/h')
def brakeCar():
    for x in range(0,5):
        userCar.brake()
        print (userCar.get_velocity(), 'km/h')

accelerateCar()
brakeCar()