# Name: Joey Carberry
# Date: October 6, 2016
# Project: car.py for project6Car.py

class Car:
    def __init__(self, year_model, make, velocity):
        self.__year_model = year_model
        self.__make = make
        self.__velocity = int(velocity)

    def get_velocity(self):
        return self.__velocity

    def accelerate(self):
        self.__velocity = self.__velocity + 5
    def brake(self):
        self.__velocity = self.__velocity - 5