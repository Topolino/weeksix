# Name: Joey Carberry
# Date: October 5, 2015
# Project: Project Five, calling is_prime function from module

import isPrime

num = 13
if (isPrime.is_prime(num)):
    print (num, 'is prime')
else:
    print (num, 'is not prime')