# Name: Joey Carberry
# Date: October 6, 2016
# Project: Project Six

import pet

myPet = pet.Pet('Boo','Probably a Dog',2)
print('The name of my pet is ', myPet.get_name())
print('My type of my animal is ', myPet.get_animal_type())
print('The age of my pet is', str(myPet.get_age()))
new_name = input('Enter a new name for your pet: ')
myPet.set_name(new_name)
new_animal_type = input('Enter a new type for your pet: ')
myPet.set_animal_type(new_animal_type)
new_age = input('Enter a new age for your pet: ')
myPet.set_age(new_age)
print('The new name of my pet is ', myPet.get_name())
print('The new animal type is ', myPet.get_animal_type())
print('The new animal age is ', str(myPet.get_age()))